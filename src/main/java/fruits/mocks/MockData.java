package fruits.mocks;

import java.util.ArrayList;
import java.util.List;

public class MockData {
	private List<String> fruits;

	public MockData() {
		initFruits();
	}

	private void initFruits() {
		fruits = new ArrayList<>();

		fruits.add("apples");
		fruits.add("pears");
		fruits.add("lemons");
	}

	public List<String> getFruits() {
		return fruits;
	}

	public void addFruit(String fruit) {
		fruits.add(fruit);
	}

	public void deleteFruit(String fruitName) {
		fruits.remove(fruitName);
	}
}
