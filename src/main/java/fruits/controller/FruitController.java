package fruits.controller;

import fruits.service.FruitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/fruits")
public class FruitController {
	@Autowired
	private FruitService fruitService;

	@GetMapping(value = "/getFruits")
	public List<String> getFruitsStringList() {
		return fruitService.getFruits();
	}

	@PostMapping(value = "/addFruit")
	public String addFruit(@RequestBody String fruit) {
		return fruitService.addFruit(fruit);
	}

	@DeleteMapping(value = "/deleteFruit/{fruitName}")
	public String deleteFruit(@PathVariable String fruitName) {
		return fruitService.deleteFruit(fruitName);
	}
}
