package fruits.service;

import fruits.mocks.MockData;
import org.springframework.stereotype.Service;

import java.text.MessageFormat;
import java.util.List;

@Service
public class FruitService {
	private static final String ERROR_ON_DELETE_FRUIT = "Could not delete.Fruit does not exist!";
	private static final String SUCCESS_ON_DELETE_FRUIT = "Deleted";
	private static final String ERROR_ON_ADD_FRUIT = "Fruit {0} already exists!";
	private static final String SUCCESS_ON_ADD_FRUIT = "Added successfully";

	private MockData mockData;

	public FruitService() {
		mockData = new MockData();
	}

	public String addFruit(String newFruit) {

		boolean fruitExistsInList = fruitAlreadyExists(newFruit);

		if (!fruitExistsInList) {
			mockData.addFruit(newFruit);
			return SUCCESS_ON_ADD_FRUIT;
		}
		return MessageFormat.format(ERROR_ON_ADD_FRUIT, newFruit);
	}

	public List<String> getFruits() {
		return mockData.getFruits();
	}

	public String deleteFruit(String fruitName) {
		if(fruitAlreadyExists(fruitName)) {
			mockData.deleteFruit(fruitName);
			return SUCCESS_ON_DELETE_FRUIT;
		}
		return ERROR_ON_DELETE_FRUIT;
	}

	private boolean fruitAlreadyExists(String newFruit) {
		List<String> fruits = mockData.getFruits();

		return fruits.stream().filter(fruit -> fruit.equals(newFruit)).findFirst().isPresent();
	}
}
